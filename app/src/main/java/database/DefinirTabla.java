package database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class Usuario implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USUARIO = "usuario";
        public static final String COLUMN_NAME_CLAVE = "clave";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
    }
}

